It is a shop by NAZAR CORPORATION

Create database with name 'nazarshop'

Create (offer, image) tables

//Create offer table

CREATE TABLE `nazarshop`.`offer` (
  `id` INT ZEROFILL UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(225) NOT NULL,
  `description` VARCHAR(2255) NULL,
  `typeId` INT NOT NULL,
  `subtypeId` INT NOT NULL,
  `sex` VARCHAR(45) NULL DEFAULT 'both',
  `color` VARCHAR(225) NULL,
  `companyId` INT NULL,
  `specifications` varchar(2255) NULL,
  `price` INT NOT NULL,
  `oldPrice` INT NULL,
  `count` INT NULL,
  `status` INT NULL DEFAULT 1,
  `createrId` INT NOT NULL,
  `updaterId` INT NULL,
  `createDate` DATE NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

// Create image table

CREATE TABLE `nazarshop`.`image` (
  `id` INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ,
ADD UNIQUE INDEX `id_UNIQUE` (`id` ASC)
  `url` VARCHAR(255) NOT NULL,
  `offerId` INT NOT NULL,
  PRIMARY KEY (`id`));