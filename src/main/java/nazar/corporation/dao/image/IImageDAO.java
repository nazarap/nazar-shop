package nazar.corporation.dao.image;

import nazar.corporation.dao.DAOInterface;
import nazar.corporation.domain.Image;
import nazar.corporation.mapper.ImageMapper;

import java.util.List;

/**
 * Created by NazaRcor on 15.09.16.
 */

public interface IImageDAO extends DAOInterface<Image> {
    public List<Image> getListByOffer(long offerId);

    public int deleteByOfferId(long offerId);
}