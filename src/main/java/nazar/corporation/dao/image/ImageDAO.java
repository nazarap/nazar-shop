package nazar.corporation.dao.image;

import nazar.corporation.domain.Image;
import nazar.corporation.mapper.ImageMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by NazaRcor on 15.09.16.
 */

public class ImageDAO implements IImageDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final String TABLE_NAME = "image";

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public long create(Image image) {
        String SQL = "insert into " + TABLE_NAME + " (url, offerId) values (?, ?)";

        return jdbcTemplateObject.update( SQL, image.getUrl(), image.getOfferId());
    }

    @Override
    public Image getOne(long id) {
        String SQL = "select * from " + TABLE_NAME + " where id = ?";
        try {
            return jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{id}, new ImageMapper());
        } catch (EmptyResultDataAccessException ex){}

        return null;
    }

    @Override
    public List<Image> getList() {
        String SQL = "select * from " + TABLE_NAME;
        return jdbcTemplateObject.query(SQL,
                new ImageMapper());
    }

    @Override
    public List<Image> getListByOffer(long offerId) {
        String SQL = "select * from " + TABLE_NAME + " where offerId = ?";
        return jdbcTemplateObject.query(SQL,
                new Object[]{offerId}, new ImageMapper());
    }

    @Override
    public int delete(long id){
        String SQL = "delete from " + TABLE_NAME + " where id = ?";
        return jdbcTemplateObject.update(SQL, id);
    }

    @Override
    public int deleteByOfferId(long offerId){
        String SQL = "delete from " + TABLE_NAME + " where offerId = ?";
        return jdbcTemplateObject.update(SQL, offerId);
    }

    @Override
    public int update(Image image, long id){
        String SQL = "update " + TABLE_NAME + " set url = ? where id = ?";
        jdbcTemplateObject.update(SQL, image.getUrl(), id);
        return 1;
    }

}