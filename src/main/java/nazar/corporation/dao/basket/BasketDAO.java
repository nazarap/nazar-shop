package nazar.corporation.dao.basket;

import nazar.corporation.domain.Basket;

import nazar.corporation.mapper.BasketMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.sql.DataSource;
import java.util.List;

/**
 * Created by domain on 16.09.16.
 */
public class BasketDAO implements IBasketDAO{

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private String tableName = "basket";

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public long create(Basket basket) {
        String SQL = "insert into " + tableName + " (name, age) values (?, ?)";

//        jdbcTemplateObject.update( SQL, name, age);
//        System.out.println("Created Record Name = " + name + " Age = " + age);
        return 1;
    }


    public Basket getOne(long id) {
        String SQL = "select * from " + tableName + " where id = ?";


        try {
            return jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{id}, new BasketMapper());
        } catch (EmptyResultDataAccessException ex){}

        return null;
    }

    public List<Basket> getList() {
        String SQL = "select * from " + tableName;
        return jdbcTemplateObject.query(SQL,
                new BasketMapper());
    }

    public int delete(long id){
        String SQL = "delete from " + tableName + " where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return 1;
    }

    public int update(Basket basket, long id){
        String SQL = "update " + tableName + " set age = ? where id = ?";
//        jdbcTemplateObject.update(SQL, age, id);
        System.out.println("Updated Record with ID = " + id );
        return 1;
    }

}