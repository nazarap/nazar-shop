package nazar.corporation.dao.basket;

import nazar.corporation.dao.DAOInterface;
import nazar.corporation.domain.Basket;


/**
 * Created by domain on 16.09.16.
 */
public interface IBasketDAO  extends DAOInterface<Basket> {
}