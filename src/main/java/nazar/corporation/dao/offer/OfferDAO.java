package nazar.corporation.dao.offer;

/**
 * Created by NazaRcor on 14.09.16.
 */
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

import java.sql.Connection;
import nazar.corporation.domain.Offer;
import nazar.corporation.mapper.OfferMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class OfferDAO implements IOfferDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final String TABLE_NAME = "offer";

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public long create(final Offer offer) {
        final String SQL = "insert into " + TABLE_NAME + " (title, description, specifications, " +
                "color, companyId, price, oldPrice, status, " +
                "count, sex, typeId, subtypeId, createrId, createDate) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplateObject.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps =
                                connection.prepareStatement(SQL, new String[] {"id"});
                        ps.setString(1, offer.getTitle());
                        ps.setString(2, offer.getDescription());
                        ps.setString(3, offer.getSpecifications());
                        ps.setString(4, offer.getColor());
                        ps.setLong(5, offer.getCompanyId());
                        ps.setInt(6, offer.getPrice());
                        ps.setInt(7, offer.getOldPrice());
                        ps.setInt(8, offer.getStatus());
                        ps.setInt(9, offer.getCount());
                        ps.setString(10, offer.getSex());
                        ps.setLong(11, offer.getTypeId());
                        ps.setLong(12, offer.getSubtypeId());
                        ps.setLong(13, offer.getCreaterId());
                        ps.setDate(14, new java.sql.Date(new Date().getTime()));
                        return ps;
                    }
                },
                keyHolder);
        System.out.print("generate gey = " + keyHolder.getKey());
        return (long) keyHolder.getKey();
    }

    @Override
    public Offer getOne(long id) {
        String SQL = "select * from " + TABLE_NAME + " where id = ?";

        Offer offer = null;

        try {
            offer = jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{id}, new OfferMapper());
        } catch (EmptyResultDataAccessException ex){}

        return offer;
    }

    @Override
    public List<Offer> getList() {
        String SQL = "select * from " + TABLE_NAME;
        List <Offer> offers = jdbcTemplateObject.query(SQL,
                new OfferMapper());
        return offers;
    }

    @Override
    public int delete(long id){
        String SQL = "delete from " + TABLE_NAME + " where id = ?";
        return jdbcTemplateObject.update(SQL, id);
    }

    @Override
    public int update(Offer offer, long id){
        String SQL = "update " + TABLE_NAME +
                " set title = ? ," +
                " description = ? ," +
                " specifications = ? ," +
                " color = ? ," +
                " companyId = ? ," +
                " price = ? ," +
                " oldPrice = ? ," +
                " status = ? ," +
                " count = ? ," +
                " sex = ? ," +
                " typeId = ? ," +
                " subtypeId = ? ," +
                " updaterId = ? " +
                "where id = ?";

        return jdbcTemplateObject.update( SQL, offer.getTitle(), offer.getDescription(), offer.getSpecifications(),
                offer.getColor(), offer.getCompanyId(), offer.getPrice(), offer.getOldPrice(), offer.getStatus(),
                offer.getCount(), offer.getSex(), offer.getTypeId(), offer.getSubtypeId(), offer.getUpdaterId(), id);
    }

}