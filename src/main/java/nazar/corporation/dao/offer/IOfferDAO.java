package nazar.corporation.dao.offer;

/**
 * Created by NazaRcor on 14.09.16.
 */
import nazar.corporation.dao.DAOInterface;
import nazar.corporation.domain.Offer;

public interface IOfferDAO extends DAOInterface<Offer> {
}