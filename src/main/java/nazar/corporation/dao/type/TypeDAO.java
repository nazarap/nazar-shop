package nazar.corporation.dao.type;

import nazar.corporation.domain.Type;
import nazar.corporation.mapper.TypeMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by NazaRcor on 30.09.16.
 */
public class TypeDAO implements ITypeDAO{

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final String TABLE_NAME = "type";

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public long create(Type type) {
        String SQL = "insert into " + TABLE_NAME + " (title) values (?)";

        return jdbcTemplateObject.update( SQL, type.getTitle());
    }

    @Override
    public int delete(long id) {
        String SQL = "delete from " + TABLE_NAME + " where id = ?";
        return jdbcTemplateObject.update(SQL, id);
    }

    @Override
    public int update(Type type, long id) {
        String SQL = "update " + TABLE_NAME +
                " set title = ? " +
                "where id = ?";

        return jdbcTemplateObject.update( SQL, type.getTitle(), id);
    }

    @Override
    public Type getOne(long id) {
        return null;
    }

    @Override
    public List<Type> getList() {
        String SQL = "select * from " + TABLE_NAME;
        List <Type> types = jdbcTemplateObject.query(SQL,
                new TypeMapper());
        return types;
    }
}
