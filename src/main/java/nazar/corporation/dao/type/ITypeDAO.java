package nazar.corporation.dao.type;

import nazar.corporation.dao.DAOInterface;
import nazar.corporation.domain.Offer;
import nazar.corporation.domain.Type;

/**
 * Created by NazaRcor on 30.09.16.
 */
public interface ITypeDAO extends DAOInterface<Type> {
}
