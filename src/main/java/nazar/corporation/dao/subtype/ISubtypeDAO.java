package nazar.corporation.dao.subtype;

import nazar.corporation.dao.DAOInterface;
import nazar.corporation.domain.Subtype;

import java.util.List;

/**
 * Created by nazar on 10/2/16.
 */
public interface ISubtypeDAO extends DAOInterface<Subtype>{
    List<Subtype> getListByType(long typeId);

    int deleteByType(long typeId);
}
