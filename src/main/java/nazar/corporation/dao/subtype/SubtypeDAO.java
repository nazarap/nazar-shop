package nazar.corporation.dao.subtype;

import nazar.corporation.domain.Subtype;
import nazar.corporation.domain.Type;
import nazar.corporation.mapper.SubtypeMapper;
import nazar.corporation.mapper.TypeMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by nazar on 10/2/16.
 */
public class SubtypeDAO implements ISubtypeDAO{

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private static final String TABLE_NAME = "subtype";

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public long create(Subtype subtype) {
        String SQL = "insert into " + TABLE_NAME + " (title, description, typeId) values (?, ?, ?)";

        return jdbcTemplateObject.update( SQL, subtype.getTitle(), subtype.getDescription(), subtype.getTypeId());
    }

    @Override
    public int delete(long id) {
        String SQL = "delete from " + TABLE_NAME + " where id = ?";
        return jdbcTemplateObject.update(SQL, id);
    }

    @Override
    public int update(Subtype subtype, long id) {
        String SQL = "update " + TABLE_NAME +
                " set title = ?, " +
                " typeId = ?, " +
                " description = ? " +
                "where id = ?";

        return jdbcTemplateObject.update( SQL, subtype.getTitle(), subtype.getTypeId(), subtype.getDescription(), id);
    }

    @Override
    public Subtype getOne(long id) {
        return null;
    }

    @Override
    public List<Subtype> getList() {
        String SQL = "select * from " + TABLE_NAME;

        List <Subtype> subtypes = jdbcTemplateObject.query(SQL,
                new SubtypeMapper());
        return subtypes;
    }

    @Override
    public List<Subtype> getListByType(long typeId) {
        String SQL = "select * from " + TABLE_NAME + " where typeId = ?";

        return jdbcTemplateObject.query(SQL,
                new Object[]{typeId}, new SubtypeMapper());
    }

    @Override
    public int deleteByType(long typeId) {
        String SQL = "delete from " + TABLE_NAME + " where typeId = ?";
        return jdbcTemplateObject.update(SQL, typeId);
    }
}
