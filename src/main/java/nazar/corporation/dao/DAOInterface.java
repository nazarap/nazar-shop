package nazar.corporation.dao;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by NazaRcor on 15.09.16.
 */
public interface DAOInterface<Model> {

    /**
     * This is the method to be used to initialize
     * database resources ie. connection.
     */
    public void setDataSource(DataSource dataSource);
    /**
     * This is the method to be used to create
     * a record in the Offer table.
     */
    public long create(Model model);
    /**
     * This is the method to be used to delete
     * a record from the Offer table corresponding
     * to a passed Offer id.
     */
    public int delete(long id);
    /**
     * This is the method to be used to update
     * a record into the Offer table.
     */
    public int update(Model m, long id);
    /**
     * This is the method to be used to list down
     * a record from the MODEL table corresponding
     * to a passed MODEL id.
     */
    public Model getOne(long id);
    /**
     * This is the method to be used to list down
     * all the records from the MODEL table.
     */
    public List<Model> getList();
}
