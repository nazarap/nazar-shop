package nazar.corporation.domain;

/**
 * Created by NazaRcor on 14.09.16.
 */
public class Subtype {

    private long id;
    private long typeId;
    private String title;
    private String description;

    public Subtype(long typeId, String title, String description) {
        this.typeId = typeId;
        this.title = title;
        this.description = description;
    }

    public Subtype(long id, long typeId, String title, String description) {
        this.id = id;
        this.typeId = typeId;
        this.title = title;
        this.description = description;
    }

    public Subtype() {}

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTypeId() {
        return this.typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

}
