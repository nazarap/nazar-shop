package nazar.corporation.domain;

/**
 * Created by NazaRcor on 14.09.16.
 */
public class Type {

    private long id;
    private String title;

    public Type(String title) {
        this.title = title;
    }

    public Type(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Type() {}

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }
}
