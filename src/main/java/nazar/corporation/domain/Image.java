package nazar.corporation.domain;

/**
 * Created by NazaRcor on 14.09.16.
 */
public class Image {

    private long id;
    private String url;
    private long offerId;

    public Image() {}

    public Image(long id) {
        this.id = id;
    }

    public Image(long id, String url, long offerId) {
        this.id = id;
        this.url = url;
        this.offerId = offerId;
    }

    public Image(String url, long offerId) {
        this.url = url;
        this.offerId = offerId;
    }

    public long getId() {
        return this.id;
    }

    public String getUrl() {
        return this.url;
    }

    public long getOfferId() {
        return this.offerId;
    }
}
