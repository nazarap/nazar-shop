package nazar.corporation.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by NazaRcor on 14.09.16.
 */
public class Offer {

    private long id;
    private long typeId;
    private long subtypeId;
    private long createrId;
    private long updaterId;
    private int status;
    private int price;
    private int oldPrice;
    private int count;
    private List<Image> images;
    private String title;
    private String description;
    private String specifications;
    private String sex;
    private String color;
    private long companyId;
    private Date createDate;
    private String[] base64s;

    public Offer() {}

    public Offer(long id) {
        this.id = id;
    }

    public Offer(String title, String description, String specifications, String color,
                 long companyId, int price, int oldPrice, int status, int count,
                 String sex, long typeId, long subtypeId, long createrId, List<Image> images) {
        this.title = title;
        this.description = description;
        this.specifications = specifications;
        this.color = color;
        this.companyId = companyId;
        this.price = price;
        this.oldPrice = oldPrice;
        this.status = status;
        this.count = count;
        this.sex = sex;
        this.typeId = typeId;
        this.subtypeId = subtypeId;
        this.createrId = createrId;
        this.images = images;
    }

    public Offer(String title, String description, String specifications, String color,
                 long companyId, int price, int oldPrice, int status, int count,
                 String sex, long typeId, long subtypeId, long createrId, String[] base64s) {
        this.title = title;
        this.description = description;
        this.specifications = specifications;
        this.color = color;
        this.companyId = companyId;
        this.price = price;
        this.oldPrice = oldPrice;
        this.status = status;
        this.count = count;
        this.sex = sex;
        this.typeId = typeId;
        this.subtypeId = subtypeId;
        this.createrId = createrId;
        this.base64s = base64s;
    }

    public void setId(long id) {
        this.id = id;
    }
    public long getId() {
        return id;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }
    public long getTypeId() {
        return typeId;
    }

    public void setSubtypeId(long subtypeId) {
        this.subtypeId = subtypeId;
    }
    public long getSubtypeId() {
        return subtypeId;
    }

    public void setCreaterId(long createrId) {
        this.createrId = createrId;
    }
    public long getCreaterId() {
        return createrId;
    }

    public void setUpdaterId(long updaterId) {
        this.updaterId = updaterId;
    }
    public long getUpdaterId() {
        return updaterId;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public int getPrice() {
        return price;
    }

    public void setOldPrice(int oldPrice) {
        this.oldPrice = oldPrice;
    }
    public int getOldPrice() {
        return oldPrice;
    }

    public void setCount(int count) {
        this.count = count;
    }
    public int getCount() {
        return count;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
    public List<Image> getImages() {
        return images;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getSex() {
        return sex;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }
    public String getSpecifications() {
        return specifications;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getColor() {
        return color;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }
    public long getCompanyId() {
        return companyId;
    }

    public void setBase64s(String[] base64s) {
        this.base64s = base64s;
    }
    public String[] getBase64s() {
        return this.base64s;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public Date getCreateDate() {
        return createDate;
    }
}
