package nazar.corporation.domain;

/**
 * Created by NazaRcor on 14.09.16.
 */
public class Basket {

    int id;
    int userId;
    int offerId;
    public Basket() {}

    public Basket(int id, int userId, int offerId) {
        this.id = id;
        this.userId = userId;
        this.offerId = offerId;
    }

    public Basket(int userId, int offerId) {
        this.userId = userId;
        this.offerId = offerId;
    }

    public int getId() {
        return this.id;
    }

    public int getUserId() {
        return this.userId;
    }

    public int getOfferId() {
        return this.offerId;
    }
}