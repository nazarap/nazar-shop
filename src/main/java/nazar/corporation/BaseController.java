package nazar.corporation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class BaseController {

    @RequestMapping(value = "/**", method = RequestMethod.GET)
    public ModelAndView index(HttpSession session, HttpServletRequest request) {
        ModelAndView result = new ModelAndView("index");
        result.addObject("path", request.getContextPath());
        return result;
    }
}