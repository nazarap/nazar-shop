package nazar.corporation.rest;

import nazar.corporation.dao.type.TypeDAO;
import nazar.corporation.domain.Type;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by NazaRcor on 30.09.16.
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class TypeController {
    private ApplicationContext context =
            new ClassPathXmlApplicationContext("Beans.xml");
    private TypeDAO typeTemplate =
            (TypeDAO)context.getBean("typeDAO");
    private SubtypeController subtypeController = new SubtypeController();

    @RequestMapping("/types")
    public List<Type> types() {
        return typeTemplate.getList();
    }

    @RequestMapping(value = "/type/new", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public long newType(@RequestBody Type type) {
        return typeTemplate.create(type);
    }

    @RequestMapping(value = "/type/delete/{id}", method = RequestMethod.DELETE)
    public long deleteType(@PathVariable(value="id") long id) {
        subtypeController.deleteByType(id);
        return typeTemplate.delete(id);
    }

    @RequestMapping(value = "/type/{id}", method = RequestMethod.PUT)
    public long updateType(@PathVariable(value="id") long id, @RequestBody Type type) {
        return typeTemplate.update(type, id);
    }

}
