package nazar.corporation.rest;

import nazar.corporation.dao.basket.BasketDAO;
import nazar.corporation.domain.Basket;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by domain on 16.09.16.
 */
@RestController
public class BasketController {
    private ApplicationContext context =
            new ClassPathXmlApplicationContext("Beans.xml");
    private BasketDAO basketTemplate =
            (BasketDAO)context.getBean("basketDAO");


    @RequestMapping("/baskets")
    public List<Basket> baskets() {
        return basketTemplate.getList();
    }

    @RequestMapping("/basket/{id}")
    public Basket busket(@PathVariable(value="id") int id) {
        return basketTemplate.getOne(id);
    }

}