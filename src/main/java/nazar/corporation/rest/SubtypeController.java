package nazar.corporation.rest;

import nazar.corporation.dao.subtype.SubtypeDAO;
import nazar.corporation.domain.Subtype;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by nazar on 10/2/16.
 */

@CrossOrigin(maxAge = 3600)
@RestController
public class SubtypeController {

    private ApplicationContext context =
            new ClassPathXmlApplicationContext("Beans.xml");
    private SubtypeDAO subtypeTemplate =
            (SubtypeDAO)context.getBean("subtypeDAO");

    @RequestMapping("/subtypes")
    public List<Subtype> subtypes(@RequestParam(value="typeId", required=false, defaultValue="0") long typeId) {
        if(typeId == 0) {
            return subtypeTemplate.getList();
        } else {
            return subtypeTemplate.getListByType(typeId);
        }
    }

    @RequestMapping(value = "/subtype/new", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public long newSubtype(@RequestBody Subtype subtype) {
        return subtypeTemplate.create(subtype);
    }

    @RequestMapping(value = "/subtype/{id}", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public long updateSubtype(@PathVariable(value="id") long id, @RequestBody Subtype subtype) {
        return subtypeTemplate.update(subtype, id);
    }

    @RequestMapping(value = "/subtype/delete/{id}", method = RequestMethod.DELETE)
    public long deleteSubtype(@PathVariable(value="id") long id) {
        return subtypeTemplate.delete(id);
    }

    long deleteByType(long typeId) {
        return subtypeTemplate.deleteByType(typeId);
    }


}
