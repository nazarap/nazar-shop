package nazar.corporation.rest;

/**
 * Created by NazaRcor on 14.09.16.
 */
import java.util.List;

import nazar.corporation.dao.offer.OfferDAO;
import nazar.corporation.domain.Offer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
public class OfferController {

    private ApplicationContext context =
            new ClassPathXmlApplicationContext("Beans.xml");
    private OfferDAO offerTemplate =
            (OfferDAO)context.getBean("offerDAO");
    private ImageController imageController = new ImageController();

    @RequestMapping("/offers")
    public List<Offer> offers() {
        return offerTemplate.getList();
    }

    @RequestMapping("/offer/{id}")
    public ResponseEntity<Offer> offer(@PathVariable(value="id") int id) {
        return new ResponseEntity<Offer>(offerTemplate.getOne(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/offer/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Offer> offerDelete(@PathVariable(value="id") long id) {
        Integer status = offerTemplate.delete(id);
        imageController.deleteByOfferId(id);
        if(status == 0) {
            return new ResponseEntity<Offer>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<Offer>(new Offer(id), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/offer/new", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public long newOffer(@RequestBody Offer offer) {
        long id = offerTemplate.create(offer);
        imageController.newImage(offer.getBase64s(), id);
        return id;
    }

    @RequestMapping(value = "/offer/update/{id}", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public int updateOffer(@PathVariable(value="id") int id, @RequestBody Offer offer) {
        return offerTemplate.update(offer, id);
    }

}