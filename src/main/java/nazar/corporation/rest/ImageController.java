package nazar.corporation.rest;

/**
 * Created by NazaRcor on 15.09.16.
 */
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Random;

import nazar.corporation.dao.image.ImageDAO;
import nazar.corporation.domain.Image;
import nazar.corporation.domain.Offer;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
public class ImageController {

    private ApplicationContext context =
            new ClassPathXmlApplicationContext("Beans.xml");
    private ImageDAO imageTemplate =
            (ImageDAO)context.getBean("imageDAO");


    @RequestMapping("/images")
    public List<Image> images(@RequestParam(value="offerId", required=false, defaultValue="0") long offerId) {
        if(offerId == 0) {
            return imageTemplate.getList();
        } else {
            return imageTemplate.getListByOffer(offerId);
        }
    }

    @RequestMapping("/image/{id}")
    public Image image(@PathVariable(value="id") int id) {
        return imageTemplate.getOne(id);
    }

    @RequestMapping(value = "/image/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Image> deleteImage(@PathVariable(value="id") int id) {
        int status = imageTemplate.delete(id);
        if(status == 0) {
            return new ResponseEntity<Image>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<Image>(new Image(id), HttpStatus.OK);
        }
    }

    public ResponseEntity newImage(String[] base64s, long offerId) {
        try
        {
            for(String base64 : base64s) {
                byte[] imageByte = Base64.decodeBase64(base64);

                String imageName = new Date().getTime() +
                        new Random().nextInt(255) + ".jpg";

                new FileOutputStream("src/main/resources/images/" + imageName).write(imageByte);

                Image image = new Image("images/" + imageName, offerId);

                imageTemplate.create(image);
            }
            return new ResponseEntity(HttpStatus.OK);
        }
        catch(Exception e)
        {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    public int deleteByOfferId(long offerId) {
        return imageTemplate.deleteByOfferId(offerId);
    }
}