package nazar.corporation.mapper;

import nazar.corporation.domain.Basket;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by domain on 16.09.16.
 */
public class BasketMapper implements RowMapper<Basket> {
    public Basket mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Basket(rs.getInt("id"), rs.getInt("userId"), rs.getInt("offerId"));
    }
}