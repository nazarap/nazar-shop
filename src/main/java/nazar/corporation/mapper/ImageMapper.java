package nazar.corporation.mapper;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;

import nazar.corporation.domain.Image;
import org.springframework.jdbc.core.RowMapper;

public class ImageMapper implements RowMapper<Image> {
    public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Image(rs.getLong("id"), rs.getString("url"), rs.getLong("offerId"));
    }
}