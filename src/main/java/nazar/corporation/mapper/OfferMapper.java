package nazar.corporation.mapper;

/**
 * Created by NazaRcor on 14.09.16.
 */

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;

import nazar.corporation.domain.Offer;
import nazar.corporation.rest.ImageController;
import org.springframework.jdbc.core.RowMapper;

public class OfferMapper implements RowMapper<Offer> {
    private ImageController imageController = new ImageController();

    public Offer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Offer offer = new Offer();
        offer.setId(rs.getLong("id"));
        offer.setTypeId(rs.getLong("typeId"));
        offer.setSubtypeId(rs.getLong("subtypeId"));
        offer.setCreaterId(rs.getLong("createrId"));
        offer.setUpdaterId(rs.getLong("updaterId"));
        offer.setStatus(rs.getInt("status"));
        offer.setPrice(rs.getInt("price"));
        offer.setOldPrice(rs.getInt("oldPrice"));
        offer.setCount(rs.getInt("count"));
        offer.setImages(imageController.images(rs.getLong("id")));
        offer.setTitle(rs.getString("title"));
        offer.setDescription(rs.getString("description"));
        offer.setSpecifications(rs.getString("specifications"));
        offer.setColor(rs.getString("color"));
        offer.setCompanyId(rs.getLong("companyId"));
        offer.setSex(rs.getString("sex"));
        offer.setCreateDate(rs.getDate("createDate"));
        try {
            Array z = rs.getArray("base64s");
            String[] base64s = (String []) z.getArray();
            offer.setBase64s(base64s);
        } catch (SQLException ex) {}

        return offer;
    }
}