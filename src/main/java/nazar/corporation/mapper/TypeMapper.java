package nazar.corporation.mapper;

import nazar.corporation.domain.Type;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by NazaRcor on 30.09.16.
 */
public class TypeMapper implements RowMapper<Type> {
    public Type mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Type(rs.getLong("id"), rs.getString("title"));
    }
}
