package nazar.corporation.mapper;

import nazar.corporation.domain.Subtype;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by NazaRcor on 10/2/16.
 */
public class SubtypeMapper implements RowMapper<Subtype> {
    public Subtype mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Subtype(rs.getLong("id"), rs.getLong("typeId"), rs.getString("title"), rs.getString("description"));
    }
}