var app = angular.module("app", ['naif.base64']);

app.controller("rootCtrl", function($scope, $http) {

    $scope.newType = {};
    $scope.subtypesListByType = {};

    var getList = function() {
        $http.get("http://localhost:8080/offers").then(function(data) {
            $scope.offersList = data.data
        });
    }
    getList();

    var getTypeList = function() {
        $http.get("http://localhost:8080/types").then(function(data) {
            $scope.typesList = data.data

            angular.forEach($scope.typesList, function(value) {
                getSubtypeListByType(value.id);
            });
        });
    }
    getTypeList();

    var getSubtypeListByType = function(typeId) {
        $http.get("http://localhost:8080/subtypes?typeId=" + typeId).then(function(data) {
            $scope.subtypesListByType[typeId] = data.data;
        });
    }

    var getSubtypeList = function(typeId) {
        $http.get("http://localhost:8080/subtypes?typeId=" + typeId).then(function(data) {
            $scope.subtypesList = data.data;
        });
    }

    $scope.setType = function() {
        getSubtypeList($scope.newOffer.typeId);
    }

    $scope.getOfferData = function() {
        $http.get("http://localhost:8080/offer/" + $scope.offerId).then(function(data) {
            $scope.offerData = data.data
        });
    }

    $scope.submit = function() {
        $scope.newOffer.base64s = [$scope.image1.base64, $scope.image2.base64, $scope.image3.base64, $scope.image4.base64];

        $http.post("http://localhost:8080/offer/new", $scope.newOffer).then(function() {getList();});
    }

    $scope.submitType = function() {
        $http.post("http://localhost:8080/type/new", $scope.newType).then(function() {
            getTypeList();
        });
    }

    $scope.submitSubtype = function() {
        $http.post("http://localhost:8080/subtype/new", $scope.newSubtype).then(function() {
            getSubtypeListByType($scope.newSubtype.typeId);
        });
    }

    $scope.deleteOffer = function(id) {
        $http.delete("http://localhost:8080/offer/delete/" + id).then(function() {
            getList();
        });
    }

    $scope.deleteType = function(id) {
        $http.delete("http://localhost:8080/type/delete/" + id).then(function() {
            getTypeList();
        });
    }

    $scope.updateType = function(id, type) {
        $http.put("http://localhost:8080/type/" + id, type).then(function() {
            getTypeList();
        });
    }


    $scope.deleteSubtype = function(id) {
        $http.delete("http://localhost:8080/subtype/delete/" + id).then(function() {
            getSubtypeListByType(subtype.typeId);
        });
    }

    $scope.updateSubtype = function(id, subtype) {
        $http.put("http://localhost:8080/subtype/" + id, subtype).then(function() {
            getSubtypeListByType(subtype.typeId);
        });
    }
});